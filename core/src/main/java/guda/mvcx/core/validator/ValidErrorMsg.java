package guda.mvcx.core.validator;

/**
 * Created by well on 17/6/15.
 */
public class ValidErrorMsg {

    public static final String TARGET_IS_NULL="目标对象为null";
    public static final String NOT_EMPTY_MSG="不能为空";
    public static final String NOT_NULL_MSG="不能为null";
    public static final String LENGTH_ERROR_MSG="长度不正确";
    public static final String SIZE_ERROR_MSG="取值范围不正确";
    public static final String REGEX_ERROR_MSG="正则验证不通过";
    public static final String UNKOWN_VALID_TYPE="未知校验类型";

    public static final String VALID_NOT_EMPTY_TYPE_ERROR="ValidType.NotEmpty只能用在字符串,集合上";

    public static final String VALID_LENGTH_TYPE_ERROR="ValidType.Length只能用在字符串上";
    public static final String VALID_LENGTH_LESS="ValidType.Length时，minLength和maxLength必须大于0";
    public static final String VALID_LENGTH_ERROR="ValidType.Length时，minLength应该小于maxLength";
    public static final String VALID_LENGTH_NOTMATCH=LENGTH_ERROR_MSG+",长度应该在%d和%d之间";

    public static final String VALID_SIZE_LESS="ValidType.Size时，minSize和maxSize必须大于0";
    public static final String VALID_SIZE_ERROR="ValidType.Size时，minSize应该小于maxSize";
    public static final String VALID_SIZE_NOTMATCH=SIZE_ERROR_MSG+",取值范围应该在%d和%d之间";
    public static final String VALID_SIZE_TYPE_ERROR="ValidType.Size只能用在Integer或者Long类型上";

    public static final String VALID_REGEX_TYPE_ERROR="ValidType.Regex只能用在字符串类型上";
    public static final String VALID_REGEX_ERROR="ValidType.Regex时，regex属性不能为空";
    public static final String VALID_REGEX_VALUE_NULL="ValidType.Regex时，值不能为null";
    public static final String VALID_REGEX_NOT_MATCH=REGEX_ERROR_MSG+",不符合正则表达式:%s";
}
