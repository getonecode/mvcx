package guda.mvcx.core.validator;

import java.lang.reflect.Method;

/**
 * Created by well on 17/6/16.
 */
public class FieldConstraint {

    private String fieldName;

    private Class fieldClass;

    private Method readMethod;

    private  ValidType[] validTypeArray;

    private int minLength;

    private int maxLength;

    private int minSize;

    private int maxSize;

    private String regex;

    private String msg;

    private FieldConstraint parent;

    public FieldConstraint getParent() {
        return parent;
    }

    public void setParent(FieldConstraint parent) {
        this.parent = parent;
    }

    public Class getFieldClass() {
        return fieldClass;
    }

    public void setFieldClass(Class fieldClass) {
        this.fieldClass = fieldClass;
    }

    public Method getReadMethod() {
        return readMethod;
    }

    public void setReadMethod(Method readMethod) {
        this.readMethod = readMethod;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public ValidType[] getValidTypeArray() {
        return validTypeArray;
    }

    public void setValidTypeArray(ValidType[] validTypeArray) {
        this.validTypeArray = validTypeArray;
    }

    public int getMinLength() {
        return minLength;
    }

    public void setMinLength(int minLength) {
        this.minLength = minLength;
    }

    public int getMaxLength() {
        return maxLength;
    }

    public void setMaxLength(int maxLength) {
        this.maxLength = maxLength;
    }

    public int getMinSize() {
        return minSize;
    }

    public void setMinSize(int minSize) {
        this.minSize = minSize;
    }

    public int getMaxSize() {
        return maxSize;
    }

    public void setMaxSize(int maxSize) {
        this.maxSize = maxSize;
    }

    public String getRegex() {
        return regex;
    }

    public void setRegex(String regex) {
        this.regex = regex;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
