package guda.mvcx.core.validator;

/**
 * Created by well on 17/6/15.
 */
public enum ValidType {

    NotNull,NotEmpty,Size,Regex,Length;
}
