package guda.mvcx.core.validator;

import java.util.regex.Pattern;

/**
 * Created by well on 17/6/19.
 */
public class RegexValidator {

    private static final Pattern emailPattern = Pattern.compile("^[a-zA-Z0-9_-]+@[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)+$");

    private static final Pattern mobilePattern = Pattern.compile("^1[34578]\\d{9}$");

    public static boolean isEmail(String mail){
        if(mail == null){
            return false;
        }
        return emailPattern.matcher(mail).find();
    }

    public static boolean isMobile(String mobile){
        if(mobile == null){
            return false;
        }
        return mobilePattern.matcher(mobile).find();
    }


}
