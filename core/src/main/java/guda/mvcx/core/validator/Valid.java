package guda.mvcx.core.validator;

import java.lang.annotation.*;

/**
 * Created by well on 17/6/15.
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Valid {

    ValidType[] type() ;

    int minLength() default 0;

    int maxLength() default Integer.MAX_VALUE;

    int minSize() default 0;

    int maxSize() default Integer.MAX_VALUE;

    String regex() default "";

    String msg() default "";
}
