package guda.mvcx.core.validator;

import java.util.Iterator;
import java.util.Map;

/**
 * Created by well on 17/6/19.
 */
public class ValidResult {

    private boolean error;

    private Map<String,String> errorInfo;

    public ValidResult(){

    }

    public ValidResult(Map<String,String> errorInfo){
        if(errorInfo != null&& errorInfo.size()>0){
            error=true;
        }
        this.errorInfo = errorInfo;
    }

    public boolean hasError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public Map<String, String> getErrorInfo() {
        return errorInfo;
    }

    public void setErrorInfo(Map<String, String> errorInfo) {
        if(errorInfo != null&& errorInfo.size()>0){
            error=true;
        }
        this.errorInfo = errorInfo;
    }

    public String getErrorString(){
        if(errorInfo == null || errorInfo.size() ==0){
            return null;
        }
        Iterator<Map.Entry<String, String>> iterator = errorInfo.entrySet().iterator();
        StringBuilder buf  = new StringBuilder();
        while(iterator.hasNext()){
            Map.Entry<String, String> next = iterator.next();
            buf.append(String.valueOf(next.getKey())).append(":").append(String.valueOf(next.getValue())).append(";");
        }
        return buf.substring(0,buf.length()-1);
    }
}
